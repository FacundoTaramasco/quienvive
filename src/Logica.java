/**
 * Created by Spirok on 04/05/2015.
 * Clase logica de la app.
 * El planteo del ejercicio es el siguiente ('Quien vive'):
 * Se tiene un vector y se quiere saber la posicion del unico elemento
 * que logra 'sobrevivir'.
 *
 * El usuario ingresa dos numeros enteros :
 *  -uno representa el largo de un vector
 *  -el otro representa una 'marca'.
 * El vector debera ser recorrido de forma circular y se aumentara en 1
 * a un contador hasta que el mismo sea igual a 'marca', cuando esto suceda
 * se 'marcara' el elemento del vector en la posicion que corresponda, hasta
 * encontrar al ultimo elemento que sobreviva, y se informara su posicion.
 * Ej.
 * largo vector : 5
 * marca : 2
 * --[0, 0, 0, 0, 0]
 *
 *   [0, eliminado, 0, 0, 0]
 *
 *   [0, eliminado, 0, eliminado, 0], etc
 */
public class Logica {

    private int[] vector;
    private int marca;

    /**
     * Consctructor
     * @param largoVec entero largo del vector
     * @param marca entero que indica
     */
    public Logica(int largoVec, int marca) {
        setVector(new int[largoVec]);
        setMarca(marca);
    }

    // Getters
    public int[] getVector() {
        return vector;
    }

    public int getMarca() {
        return marca;
    }

    // Setters
    private void setMarca(int marca) {
        this.marca = marca;
    }

    private void setVector(int[] vector) {
        this.vector = vector;
    }

    // Customs
    public void quienVive() {
        boolean termine = false;
        int acc = 0;
        int indiceVec = 0;
        int noDisponibles = 0;
        while (!termine) {
            if (lugarDisponible(indiceVec))
                acc++;
            if (acc == getMarca()) {
                acc = 0;
                getVector()[indiceVec] = -1;
                noDisponibles++;
                // queda solo un elemento disponible
                if (noDisponibles == getVector().length - 1) {
                    for (int x = 0; x < getVector().length ; x++) {
                        if (lugarDisponible(x)) {
                            System.out.println("Vive : " + x);
                            termine = true;
                            break;
                        }
                    }
                }
            }
            // calculo indice circular
            indiceVec = (indiceVec + 1) % getVector().length;
        }
    }

    private boolean lugarDisponible(int i) {
        return (getVector()[i] == 0);
    }

}
